from logger.src import logger
from logger.src import system_logger
from logger.src.system_logger import SystemLogger

import logging
import time


"""
API:
    
    logger = SystemLogger()
    
    logger.option()
    
    logger.info('info')
    logger.info_throttle(1, 'info')
    
    
    
"""


def main():
    # testDifferentWays()
    testAdvLogging()
    # testThrottle()


def testDifferentWays():
    logging.info('info 1')
    logging.warning('warn 1')

    my_logger = logging.getLogger('my_logger')
    my_logger.setLevel(logging.INFO)
    my_logger.info('info 2')
    my_logger.warning('warn 2')

    logger.loginfo('info 3')
    logger.logwarn('warn 3')

    logging.Logger

    z=0


def testAdvLogging():
    std_logger = logging.getLogger('std_logger')
    std_logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('std_logger.log')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    std_logger.addHandler(ch)
    std_logger.addHandler(fh)

    # 'application' code
    std_logger.debug('debug message')
    std_logger.info('info message')
    std_logger.warning('warn message')
    std_logger.error('error message')
    std_logger.critical('critical message')


def testAdvLogging2():
    # set up logging to file - see previous section for more details
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M',
        filename='/temp/myapp.log',
        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    # Now, we can log to the root logger, or any other logger. First the root...
    logging.info('Jackdaws love my big sphinx of quartz.')

    # Now, define a couple of other loggers which might represent areas in your
    # application:

    logger1 = logging.getLogger('myapp.area1')
    logger2 = logging.getLogger('myapp.area2')

    logger1.debug('Quick zephyrs blow, vexing daft Jim.')
    logger1.info('How quickly daft jumping zebras vex.')
    logger2.warning('Jail zesty vixen who grabbed pay from quack.')
    logger2.error('The five boxing wizards jump quickly.')


def testThrottle():
    for x in range(100):
        logger.loginfo_throttle(1, 'x: {}'.format(x))
        time.sleep(0.1)



if __name__ == '__main__':
    main()
