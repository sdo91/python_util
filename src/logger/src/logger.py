# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

"""rospy internal core implementation library"""

import atexit

try:
    import cPickle as pickle
except ImportError:
    import pickle
import inspect
import logging
import os
import signal
import sys
import threading
import time
import traceback
import types

try:
    import urllib.parse as urlparse  # Python 3.x
except ImportError:
    import urlparse

try:
    import xmlrpc.client as xmlrpcclient  # Python 3.x
except ImportError:
    import xmlrpclib as xmlrpcclient  # Python 2.x

# number of seconds to wait to join on threads. network issue can
# cause joins to be not terminate gracefully, and it's better to
# teardown dirty than to hang
_TIMEOUT_SHUTDOWN_JOIN = 5.

#########################################################


logdebug = logging.getLogger('rosout').debug

logwarn = logging.getLogger('rosout').warning

loginfo = logging.getLogger('rosout').info

logerr = logging.getLogger('rosout').error
logerror = logerr  # alias logerr

logfatal = logging.getLogger('rosout').critical

logging.getLogger('rosout').setLevel(logging.INFO)


class LoggingThrottle(object):
    last_logging_time_table = {}

    def __call__(self, caller_id, logging_func, period, msg):
        """Do logging specified message periodically.

        - caller_id (str): Id to identify the caller
        - logging_func (function): Function to do logging.
        - period (float): Period to do logging in second unit.
        - msg (object): Message to do logging.
        """
        now = time.time()

        last_logging_time = self.last_logging_time_table.get(caller_id)

        if last_logging_time is None or (now - last_logging_time) > period:
            logging_func(msg)
            self.last_logging_time_table[caller_id] = now


_logging_throttle = LoggingThrottle()


def _frame_record_to_caller_id(frame_record):
    frame, _, lineno, _, code, _ = frame_record
    caller_id = (
        inspect.getabsfile(frame),
        lineno,
        frame.f_lasti,
    )
    return pickle.dumps(caller_id)


def logdebug_throttle(period, msg):
    caller_id = _frame_record_to_caller_id(inspect.stack()[1])
    _logging_throttle(caller_id, logdebug, period, msg)


def loginfo_throttle(period, msg):
    caller_id = _frame_record_to_caller_id(inspect.stack()[1])
    _logging_throttle(caller_id, loginfo, period, msg)


def logwarn_throttle(period, msg):
    caller_id = _frame_record_to_caller_id(inspect.stack()[1])
    _logging_throttle(caller_id, logwarn, period, msg)


def logerr_throttle(period, msg):
    caller_id = _frame_record_to_caller_id(inspect.stack()[1])
    _logging_throttle(caller_id, logerr, period, msg)


def logfatal_throttle(period, msg):
    caller_id = _frame_record_to_caller_id(inspect.stack()[1])
    _logging_throttle(caller_id, logfatal, period, msg)


#########################################################
# Logging

_log_filename = None


def configure_logging(node_name, level=logging.INFO):
    """
    Setup filesystem logging for this node
    @param node_name: Node's name
    @type  node_name str
    @param level: (optional) Python logging level (INFO, DEBUG, etc...). (Default: logging.INFO)
    @type  level: int
    """
    global _log_filename

    # #988 __log command-line remapping argument
    mappings = get_mappings()
    if '__log' in get_mappings():
        logfilename_remap = mappings['__log']
        filename = os.path.abspath(logfilename_remap)
    else:
        # fix filesystem-unsafe chars
        filename = node_name.replace('/', '_') + '.log'
        if filename[0] == '_':
            filename = filename[1:]
        if not filename:
            raise rospy.exceptions.ROSException('invalid configure_logging parameter: %s' % node_name)
    _log_filename = rosgraph.roslogging.configure_logging('rospy', level, filename=filename)


class NullHandler(logging.Handler):
    def emit(self, record):
        pass


# keep logging happy until we have the node name to configure with
logging.getLogger('rospy').addHandler(NullHandler())
